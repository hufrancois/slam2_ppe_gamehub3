package lecode;


import java.awt.Color;
import java.util.*; 
import static lecode.Sketch.figCour;
import static processing.core.PApplet.floor;
import static processing.core.PApplet.print;

public class SketchPuzzles  extends Sketch{  
        
  static List<List<Fig>>  lesPuzzles= new LinkedList();   

  boolean messageAffiche=false;
  
  int     nbPuzzleEnCours;
  long tempsD;
  long temps;
  int deplacement = 0;
  int start = 0;
  int stoppartie = 0;
  int score = 0;

  public SketchPuzzles() {
       
        LGCASES = 6;
        HTCASES = 6;
        DIMCASE =70;
        
        matrice= new Fig[LGCASES][HTCASES];     
   }
   

////////////////////////////////////   SETUP ////////////////////////////////////
    
  @Override
  public void setup(){
  
    size (LGCASES*DIMCASE, HTCASES*DIMCASE);
    smooth();
    initFigures(); 
    selectionnerPuzzle(23);
}
    
////////////////////////////////////   BOUCLE DE DESSIN   ////////////////////////

  @Override
  public void draw(){
    
    background(230);
    quadrillage();
    strokeWeight(2);
    if(stoppartie==0) {
            terminer();
            if(start == 1) {
                temps = System.currentTimeMillis() - tempsD;
                this.getControleur().setMessage("Déplacement: "+ deplacement +" | Temps: " + (float) temps / 1000+"s");
            }
            else {
                this.getControleur().setMessage("Déplacement: 0 | Temps: 0s");
            }
            
    }
    else {
        this.getControleur().setMessage("Déplacement: "+ deplacement +" | Temps: " + (float) temps / 1000+"s | Score: "+score+"pts");
    }
    for(Fig f : lesFigs) f.affiche();
   
}

////////////////////////////////////   GESTION CLAVIER    ////////////////////////

  @Override 
  public void keyPressed(){

   boolean toucheActive=false;
   switch(key){
   
       
      case 'm':  //    Affichage d'un message
        if (!messageAffiche)this.getControleur().setMessage("Hello");
        else this.getControleur().setMessage("");
        messageAffiche=!messageAffiche;
      break;   
       
       case 'c': // PUZZLE SUIVANT
           deplacement=0;
            tempsD=0l;
            start = 0;
            stoppartie=0;
            this.selectionSeqPuzzle(1);
      break;     
          
      case 'x': // PUZZLE PRECEDENT
           deplacement=0;
            tempsD=0l;
            start = 0;
            stoppartie=0;
            this.selectionSeqPuzzle(-1);
      break;      
           
      case 'p': // NOUVEAU PUZZLE PAR TIRAGE AU SORT
           deplacement=0;
            tempsD=0l;
            start = 0;
            stoppartie=0;
            this.selectionAleatPuzzle();
      break; 
       
      case 'r': // ROTATION
          toucheActive= true;
          if(figCour!=null && figCour.peutTourner() ){
            int etat=(figCour.etat+1)%4 ;
            figCour.oriente(etat) ;
            print( "rotation ");
          }         
      break;
      
      case 'a':  // ZOOM AVANT
       DIMCASE++;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
       this.redraw();
      break;
       
      case 'd': // ZOOM ARRIERE
       DIMCASE--;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
       this.redraw();
      break;
          
          
        
  }
     
  switch(keyCode){    
      case 37: // NAV GAUCHE
       toucheActive= true;     
       if(figCour!=null){
           if( !figCour.contactBordGauche() && !figCour.contactGaucheFigure()) { 
            start = 1;
            deplacement++;
            if(deplacement == 1) {
                tempsD = System.currentTimeMillis();
            }
                figCour.depX(-1); print( "dep gauche ");
            }
            else if(figCour.contactBordGauche()){
                    print( "collision bord gauche ");
                  }
                  else{
                    print( "collision gauche  avec autre figure ");
                  }
       }
       
      break;
      case 39:   // NAV DROITE
        toucheActive= true;
        if(figCour!=null){
           if( !figCour.contactBordDroit() && !figCour.contactDroitFigure()) { 
                start = 1;
                deplacement++;
            if(deplacement == 1) {
                tempsD = System.currentTimeMillis();
            }
                figCour.depX(1); print( "dep droite ");
            }
            else if(figCour.contactBordDroit()){
                    print( "collision bord droit ");
                  }
                  else{
                    print( "collision droite  avec autre figure ");
                  }
       }
      break; 
      case 38:  // NAV HAUT
         toucheActive= true; 
         
         if(figCour!=null){
           if( !figCour.contactBordHaut() && !figCour.contactHautFigure()) { 
                start = 1;
                deplacement++;
            if(deplacement == 1) {
                tempsD = System.currentTimeMillis();
            }
                figCour.depY(-1); print( "dep haut ");
            }
            else if(figCour.contactBordHaut()){
                    print( "collision bord haut ");
                  }
                  else{
                    print( "collision haut  avec autre figure ");
                  }
         }
      break;
      case 40: //  NAV BAS
         
          toucheActive= true;
         
          if(figCour!=null){
             
             if( !figCour.contactBordBas()) {
               if (!figCour.contactBasFigure()) { 
                start = 1;
                deplacement++;
            if(deplacement == 1) {
                tempsD = System.currentTimeMillis();
            }
                   figCour.depY(1); print( "dep bas ");
               }
               else{
                    print( "collision bas avec autre figure ");
               }
             }  
             else{
              print( "collision bord bas");
             }  
          }
            
      break;
   }
   if (toucheActive)afficheFigSel();
   
 }
   
  ///////////////////////////////      CREATION   DES PUZZLES EN MEMOIRE   ///////

  @Override
  public void initFigures(){
         
     
      PuzzlesData lPzd=new PuzzlesData();
      
      for ( Object[][] pzd : lPzd.getLesPuzzlesData()){
      
        List<Fig>   pz = new LinkedList();  
        
        for (Object[] descFig :pzd  ){
            
            String  type    =  (String)  descFig[0];
            int     x       =  (Integer) descFig[1];
            int     y       =  (Integer) descFig[2];
            int     etat    =  (Integer) descFig[3];
            Color   couleur =  (Color)   descFig[4]; 
        
            Fig     fig =new Fig(type,x,y,etat,couleur,this);
           
            pz.add(fig);    
        }
        
        lesPuzzles.add(pz);
      }
}    
 
 
  //////////////////////////////       SELECTION ALEATOIRE D'UN NUMEROE DE PUZZLE   TOUCHE P  /////////
  
  private void selectionAleatPuzzle() {
     
        nbPuzzleEnCours= floor(random(lesPuzzles.size()));   
        selectionnerPuzzle(nbPuzzleEnCours);

 }
  
 /////////////////////////////         SELECTION SEQUENTIELLE CYCLIQUE  D4UN NUMERO DE PUZZLE  touche C incrémenter  touche X pour décrémenter 
 
  private void selectionSeqPuzzle(int sens) {
    
        if (sens==1){nbPuzzleEnCours++;} else {if (nbPuzzleEnCours>0) nbPuzzleEnCours--;else nbPuzzleEnCours=lesPuzzles.size()-1;}
        nbPuzzleEnCours%=lesPuzzles.size();
       
        selectionnerPuzzle(nbPuzzleEnCours); 
  } 
 
  
  //////////////////////////////////    SELECTION D'UN PUZZLE   //////////////////////////////////////
  
  private void selectionnerPuzzle(int nbPuzzleEnCours){
  
     lesFigs=lesPuzzles.get(nbPuzzleEnCours); 
      
     if      (nbCases(lesFigs)!=25&& !lesFigs.isEmpty()){
             
             System.out.printf("Puzzle N° : %3d NON CONFORME\n",nbPuzzleEnCours);
             this.vue.setTitle("PUZZLE N° " +nbPuzzleEnCours+" NON CONFORME "+ nbCases(lesFigs)+" CASES");
        }
        else if (lesFigs.isEmpty() ) {
             
             System.out.printf("Puzzle N° : %3d VIDE\n",nbPuzzleEnCours);
             this.vue.setTitle("PUZZLE N° " +nbPuzzleEnCours+" VIDE");
        }
        else {
             
             System.out.printf("Puzzle N° : %3d OK\n",nbPuzzleEnCours);  
             this.vue.setTitle("PUZZLE N° " +nbPuzzleEnCours);
       }
        
       for(int c= 0;c<LGCASES;c++ )for(int l=0; l<HTCASES;l++) matrice[c][l]=null;
  
  }  
    
    public void score() {
        temps = System.currentTimeMillis() - tempsD;
        score = (int) Math.floor(500 - deplacement*2 - ((float) temps / 1000)*2);
    }
    
    public void terminer() {
        int fin = 0;
        for(int x=0;x<LGCASES-1;x++) {
            for(int y=0;y<HTCASES-1;y++) {
                if(matrice[x][y] == null) { fin = 1;  }
            }
        }
        if(fin == 0) { stoppartie = 1; score(); }
    }
    
  //////////////////////////////  CALCUL NOMBRES DE CASE Qu'OCCUPE UN PUZZLE  //////////////////////////////////////////////////  
     
    private int nbCases( List<Fig> pz){
  
     int nbCases=0;
     for ( Fig f : pz){ nbCases+=f.nbCases();}
    
     return nbCases;
    }
   
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
    //<editor-fold defaultstate="collapsed" desc="gets & sets">
    
    public int getNbPuzzleEnCours() {
        return nbPuzzleEnCours;
    }
    //</editor-fold> 
}
