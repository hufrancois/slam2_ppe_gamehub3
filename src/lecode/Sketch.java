
package lecode;

import controleur.Controleur;
import ihm.VueJeu;
import java.util.*; 
import java.awt.Color;
import processing.core.*;
import static processing.core.PApplet.print;
import static processing.core.PApplet.println;

public class Sketch  extends PApplet{  
        
///////////////////////////////////   DIMENSIONS              ////////////////////////////////

 int                          LGCASES, HTCASES, DIMCASE;

///////////////////////////////////   IDENTIFIANT FIGURE     /////////////////////////////////

 static int                   idSuivant=1;    

///////////////////////////////////   CONSTANTES DIRECTIONS   ////////////////////////////////

 static final int             DROITE=0, GAUCHE=1, HAUT=2, BAS=3;

////////////////////////////////////  DEFINITION DES COULEURS ////////////////////////////////
 
 static Color ROUGE      = Color.RED;
 static Color VERT       = Color.GREEN;
 static Color BLEU       = Color.BLUE;
 static Color JAUNE      = Color.YELLOW;
 static Color VIOLET     = Color.MAGENTA;
 static Color AQUA       = Color.cyan;
 static Color NOIR       = Color.BLACK;
 static Color OUTREMER   = Color.BLUE;
 static Color ORANGE     = Color.ORANGE;
 static Color VERTPALE   = Color.GREEN;
 static Color BLANC      = Color.WHITE;
 
 //////////////////////////////////   DEFINITION DES STRUCTURES DE DONNEES ////////////

 static Map<String, CompFig>     comps   = new HashMap(); 
 
 static String[]                 typesFigs;    


 
 Fig [][]                        matrice;
 
 List<Fig>                       lesFigs;
 
 static Fig                      figCour;  
        
 //////////////////////////////////   INITIALISATION DES COMPORTEMENTS   /////////////////////////////////////
    
 public static void initComportements(){
 
     
   comps.put("A", new CompFigA());
   comps.put("B", new CompFigB());
   comps.put("C", new CompFigC());
   comps.put("D", new CompFigD());
   comps.put("E", new CompFigE());
   comps.put("F", new CompFigF());
   comps.put("G", new CompFigG());
   comps.put("H", new CompFigH());
   comps.put("I", new CompFigI());
   comps.put("J", new CompFigJ());
   comps.put("K", new CompFigK()); 
   comps.put("L", new CompFigL()); 
   comps.put("M", new CompFigM());  
  
   
   
 }

///////////////////////////////////////////////////////////////     CREATION   DES PUZZLES EN MEMOIRE               ///////////////////

 public void initFigures(){ }    
 

/////////////////////////////////////  GESTION DE LA SOURIS   /////////////////////////////

 @Override
 public void mousePressed(){

  int mx=mouseX/DIMCASE,my=mouseY/DIMCASE; 
  
  figCour=matrice[mx][my]; 
   
  print("click en x="+mx+" y="+my);
  if   (figCour!=null){
       print(" ");
       afficheFigSel();
  }
  else    println( " Pas de figure sélectionnée");   
}        

/////////////////////////////////////  LOG CONSOLE            /////////////////////////////

 void        afficheFigSel(){

  if (figCour!=null){
    println( " Figure Id= "+figCour.id+" Type= "+figCour.type+
             " en x="+figCour.x+ " y="+figCour.y+" Etat= "
             +figCour.etat+ "  L="+figCour.largeur()+" H= "+figCour.hauteur()
             );
  } 
}

/////////////////////////////////////  DESSIN DU QUADRILLAGE  /////////////////////////////

 void        quadrillage(){

 stroke(200, 200, 200); 
 strokeWeight(1); 
  
 for (int x=0;x<DIMCASE*LGCASES; x+=DIMCASE){
    line(x,0,x,DIMCASE*HTCASES-1);
  }

 for(int y=0;y<DIMCASE*HTCASES;y+=DIMCASE){
    line (0,y,DIMCASE*LGCASES-1,y);
 }  
 
 strokeWeight(0.5f);
 stroke(180, 180, 180);

 line(0,0,DIMCASE*LGCASES,0);
 line(0,0,0,DIMCASE*HTCASES);
 line(DIMCASE*LGCASES-1,0,DIMCASE*LGCASES-1,DIMCASE*HTCASES-1);
 line(0,DIMCASE*HTCASES-1,DIMCASE*LGCASES-1,DIMCASE*HTCASES-1);

 stroke(200, 200, 200); 
 strokeWeight(1);
}
   
 
 Controleur controleur;
 VueJeu     vue;
 
 //<editor-fold defaultstate="collapsed" desc="gets & sets">
 
 public Controleur getControleur() {
     return controleur;
 }
 
 public void setControleur(Controleur controleur) {
     this.controleur = controleur;
 }
 
 public VueJeu getVue() {
     return vue;
 }
 
 public void setVue(VueJeu vue) {
     this.vue = vue;
 }
 
 //</editor-fold>
  
 public void terminer(){exit();}

}


