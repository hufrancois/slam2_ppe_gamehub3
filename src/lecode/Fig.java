package lecode;

import java.awt.Color;
import processing.core.*;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class  Fig { 
  
     /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
     int            id; 
     String         type;     // type de figure A, B, C, D E, ...
    
     int            x,y;      // coordonnees du coin supérieur gauche de la figure 
     int            etat;     // orientation 0 1 2 ou 3
     boolean        allume;
     boolean        etiquette;
     Color          couleur;
    
     CompFig        comp;     // Code gérant le Comportement de la figure 

     Sketch         pap;

     ////////////////////////////// Constructeur pour créer des figures en mémoire  ////////////////////////////////    
  
     
    public Fig(String type, int x, int y, int etat, Color couleur, Sketch pap){      
         
          this.id=Sketch.idSuivant;
          Sketch.idSuivant++;   
          
          this.type = type;    
          this.comp = Sketch.comps.get(type);
                                                        
          this.x=x;                             
          this.y=y;                             
                                               
          this.etat=etat;
          this.couleur=couleur;
          this.pap=pap;
          this.allume=true;
          this.etiquette=true;
    }
       
   
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public   void oriente(int etat)        {this.etat=etat;}  

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public final boolean contactBordDroit()  { return x==pap.LGCASES-largeur(); }
    public final boolean contactBordGauche() { return x==0;}
    public final boolean contactBordHaut()   { return y==0;}
    public final boolean contactBordBas()    { return y==pap.HTCASES-hauteur();}
    
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////   

      int hauteur() { return comp.getHauteur()[etat]; }
      int largeur() { return comp.getLargeur()[etat]; }
    
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      
    public   void  affiche(){
       
           prepareDessin();
           dessine();
     }
 
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////    

        void prepareDessin(){
         
           viderCases();
           remplirCases();  
                
        }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                
                void viderCases(){
                    
                    if (comp==null) System.out.println("COMP NULL");
                    int cote=Math.max(maxTab(comp.getLargeur()),maxTab(comp.getHauteur()));
                  
                    for (int nc=0; nc<cote;nc++){for( int nl=0; nl<cote;nl++){
                         
                         if (x+nc <pap.LGCASES &&  y+nl<pap.HTCASES) {  
                              if (  pap.matrice[x+nc][y+nl]== this) pap.matrice[x+nc][y+nl]=null;
                         }
                       }
                    }
                   
                }
       
                
                int maxTab (int[] t ){ int max=0; for (int i=0;i<t.length;i++){if(max<t[i])max=t[i];}return max;}
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
  
                void remplirCases(){
  
                    for ( int nc=0; nc< comp.getCasesARemplir()[etat].length; nc++){
                        
                       int xt= x+comp.getCasesARemplir()[etat][nc][0];
                       int yt= y+comp.getCasesARemplir()[etat][nc][1];
                      
                       if(xt<pap.LGCASES && yt< pap.HTCASES) pap.matrice[xt][yt]=this;  
                    }
                }
           
                /////////////////////////////////////////////////////////////////////////////////////////////////////   
     
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       int nbCases (){
       
         return comp.getCasesARemplir()[0].length;
       }         
                
                
       void dessine(){
        
            int alpha=100;
          
            if (!allume) alpha=20;
        
            pap.strokeWeight(0.05f);
            pap.stroke(255,0,0,0);
            
            pap.pushMatrix();
            pap.translate(x*pap.DIMCASE,y*pap.DIMCASE);  
         
            for(int nc=0;nc<comp.getCasesARemplir()[etat].length;nc++ ){
            
                float cx= (comp.getCasesARemplir()[etat][nc][0])*pap.DIMCASE;
                float cy= (comp.getCasesARemplir()[etat][nc][1])*pap.DIMCASE;
            
                pap.fill(couleur.getRed(),couleur.getGreen(),couleur.getBlue(),alpha); 
                pap.rect(cx,cy,(float)pap.DIMCASE,(float)pap.DIMCASE);
                
                if (etiquette){
                  
                  pap.fill(0,0,0,50); 
                  pap.text(type, cx+pap.DIMCASE/2, cy+pap.DIMCASE/2);
                }
            }  
            
            pap.popMatrix();
        }



   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  public     void depX(int dep)     {
         
         int sens;
         x+=dep;
         if ( dep==1) sens=Sketch.GAUCHE ;else sens=Sketch.DROITE;
         supprimerTrace(sens);
    } 
     
  public      void depY(int dep)     {
       
       int sens;  
       y+=dep;  
       if ( dep==1) sens=Sketch.HAUT ;else sens=Sketch.BAS;
       supprimerTrace(sens);
     }
      
      
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
           void supprimerTrace(int sens){
       
               for (int i=0;i<comp.getVoisins()[sens][etat].length;i++){
        
                   int xt= x + comp.getVoisins()[sens][etat][i][0];
                   int yt= y + comp.getVoisins()[sens][etat][i][1];
                   
                   pap.matrice[xt][yt]=null;
                           
               }  
      
            }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public  boolean peutTourner()         {
     
       boolean ok=true;
       
       for (int i=0; i< comp.getCasesBloquantes()[etat].length; i++ ){
       
          int xt= x + comp.getCasesBloquantes()[etat][i][0];
          int yt= y + comp.getCasesBloquantes()[etat][i][1];
          
           if (xt >=pap.LGCASES|| yt >= pap.HTCASES) {  ok=false; break;}
          
         
          Fig  f= pap.matrice[xt][yt];
                         
          if( f != null && f != this ) { ok=false; break; }
       }
     
       return ok;
     }
     
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
   public   boolean contactDroitFigure()   { return contactVoisins(Sketch.DROITE);}
   public   boolean contactGaucheFigure()  { return contactVoisins(Sketch.GAUCHE);}
   public   boolean contactHautFigure()    { return contactVoisins(Sketch.HAUT);}
   public   boolean contactBasFigure()     { return contactVoisins(Sketch.BAS);}
 
   /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
 
          boolean contactVoisins(int sens){
 
            boolean contact=false;
  
            for ( int nv=0;nv<comp.getVoisins()[sens][etat].length;nv++){
   
                 int xt= x + comp.getVoisins()[sens][etat][nv][0];
                 int yt= y + comp.getVoisins()[sens][etat][nv][1];
                 
                 if (xt >= pap.LGCASES || yt>=pap.HTCASES){contact=true;break;}
                
                 if ( pap.matrice[xt][yt] != null ){
                 
                       contact=true;
                       break;
                 }
            }
            
            return contact;
          }
          //////////////////////////////////////////////////////////////////////////////////////////////
          
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       

 public void  raboter(){
   
       if (comp.getNvType()[etat].equals("+")){return;}
           
       for ( int nc=0; nc< comp.getCasesARaboter()[etat].length; nc++){
     
             int  xt= x+comp.getCasesARaboter()[etat][nc][0];
             int  yt= y+comp.getCasesARaboter()[etat][nc][1];
         
              pap.matrice[xt][yt]=null;                 
        } 
         
        if (comp.getNvType()[etat].equals("-")){
        
            pap.lesFigs.remove(this);
            return;
        }  
           
        this.x+=comp.getNvPosition()[etat][0];
        this.y+=comp.getNvPosition()[etat][1];
           
        this.type=comp.getNvType()[etat];
        this.etat=comp.getNvEtat()[etat];
           
        this.comp=Sketch.comps.get(type);         
   }

    ////////////////////////////////////////////////////////////////////////////////////
 
    //<editor-fold defaultstate="collapsed" desc="gets & sets">
 
 public int getId() {
     return id;
 }
 
 public void setId(int id) {
     this.id = id;
 }
 
 public String getType() {
     return type;
 }
 
 public void setType(String type) {
     this.type = type;
 }
 
 public int getX() {
     return x;
 }
 
 public void setX(int x) {
     this.x = x;
 }
 
 public int getY() {
     return y;
 }
 
 public void setY(int y) {
     this.y = y;
 }
 
 public int getEtat() {
     return etat;
 }
 
 public void setEtat(int etat) {
     this.etat = etat;
 }
 
 public Color getCouleur() {
     return couleur;
 }
 
 public void setCouleur(Color couleur) {
     this.couleur = couleur;
 }
 
 public CompFig getComp() {
     return comp;
 }
 
 public void setComp(CompFig comp) {
     this.comp = comp;
 }
 
 public PApplet getPap() {
     return pap;
 }
 
 public void setPap(Sketch pap) {
     this.pap = pap;
 }
 
 
 public boolean isAllume() {
        return allume;
    }

    public void setAllume(boolean allume) {
        this.allume = allume;
    }
 
    
    
    //</editor-fold>
 
   ///////////////////////////////////////////////////////////////////////////////////// 

    public boolean isEtiqette() {
        return etiquette;
    }

    public void setEtiquette(boolean etiquette) {
        this.etiquette = etiquette;
    }
}  
