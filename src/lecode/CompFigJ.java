package lecode;

//Auteurs : Lion Kévin, Rémy Duprez et Arthur Selosse
class CompFigJ  implements CompFig{
 
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  int        largeur[]={1,4,1,4};
  int        hauteur[]={4,1,4,1};

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  
 
 
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  int[][][]     casesARemplir=
  {
    { {0,0},{0,1},{0,2},{0,3} }, // ETAT 0
    { {0,0},{1,0},{2,0},{3,0} }, // ETAT 1
    { {0,0},{0,1},{0,2},{0,3} }, // ETAT 2
    { {0,0},{1,0},{2,0},{3,0} }  // ETAT 3 
  };
   
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
   
   int[][][]     casesBloquantes=
   {
     { {1,0},{2,0},{3,0} },   // ETAT 0
     { {0,1},{0,2},{0,3} },   // ETAT 1
     { {1,0},{2,0},{3,0} },   // ETAT 2
     { {0,1},{0,2},{0,3} },   // ETAT 3
   };
 
 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
   int [][][][]  voisins=
   {
     { // A DROITE
       { {1,0},{1,1},{1,2},{1,3} },           // ETAT 0 
       { {4,0} },                             // ETAT 1
       { {1,0},{1,1},{1,2},{1,3} },           // ETAT 2
       { {4,0} }                              // ETAT 3
     },
     {// A GAUCHE
       { {-1,0},{-1,1},{-1,2},{-1,3} },       // ETAT 0 
       { {-1,0} },                            // ETAT 1
       { {-1,0},{-1,1},{-1,2},{-1,3} },       // ETAT 2
       { {-1,0} }                             // ETAT 3
     },
     {// EN HAUT
       { {0,-1} },                            // ETAT 0 
       { {0,-1},{1,-1},{2,-1},{3,-1} },       // ETAT 1
       { {0,-1} },                            // ETAT 2
       { {0,-1},{1,-1},{2,-1},{3,-1} }        // ETAT 3
     },
     {// EN BAS
       { {0,4} },                             // ETAT 0 
       { {0,1},{1,1},{2,1},{3,1} },           // ETAT 1
       { {0,4} },                             // ETAT 2
       { {0,1},{1,1},{2,1},{3,1} }            // ETAT 3
   }   
  };
  
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  
 String[]   nvType={"E","-","E","-"};
 int[]      nvEtat={ 1 , 0 , 1 , 0};
 int[][][]  casesARaboter=
            {
              { {0,3} },                // ETAT 0
              {},                       // ETAT 1
              { {0,3} },                // ETAT 2
              {}                        // ETAT 3
            };
            
 int[][]    nvPosition=
            {
              {0,0},     // ETAT 0
              {0,0},     // ETAT 1      
              {0,0},     // ETAT 2 
              {0,0}      // ETAT 3  
            };               
   
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  //<editor-fold defaultstate="collapsed" desc="getters  ">
  @Override
  public int[][][]     getCasesARemplir()   { return casesARemplir; }
  
  @Override
  public int[][][]     getCasesBloquantes() { return casesBloquantes; }
  
  @Override
  public int[][][][]   getVoisins()         { return voisins; };
  
  
  @Override
  public String[]      getNvType(){return nvType;}
  
  @Override
  public int[]         getNvEtat(){return nvEtat;}
  
  @Override
  public int[][][]     getCasesARaboter(){return casesARaboter;}
  
  @Override
  public int[][]       getNvPosition(){return nvPosition;}
  
  
  @Override
  public int[]         getLargeur(){return largeur;}
  
  @Override
  public int[]         getHauteur(){return hauteur;}
  //</editor-fold>

 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}