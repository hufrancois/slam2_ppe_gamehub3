
package lecode;

import java.util.*; 
import static lecode.Sketch.ROUGE;
import static lecode.Sketch.figCour;
import static processing.core.PApplet.print;
import static processing.core.PApplet.println;

public class SketchLightOut  extends Sketch{  
 
  List<Fig>   pz = new LinkedList();  
  int nIt=0;  
    
  public SketchLightOut() {
        
   LGCASES =  5;
   HTCASES =  5;
   DIMCASE =  84;  
   
   matrice= new Fig[LGCASES][HTCASES];
 }
 
 
////////////////////////////////////  SETUP                   ///////////////////////////////
    
 @Override
 public void setup(){
  
  size (LGCASES*DIMCASE, HTCASES*DIMCASE);
  smooth();
  frameRate(30);
  figure();
  noLoop();
}
 
public void figure(){
    for(int i=0;i<=LGCASES-1;i++) {
        for(int j=0;j<=HTCASES-1;j++) {
            Fig fig =new Fig("A",i,j,0,ROUGE,this);
            fig.allume = false;
            if(floor((float) (Math.random()*100)) <= 10) {
                fig.allume = true;
            }
            pz.add(fig);
        }
    }
}
    
////////////////////////////////////  BOUCLE DE DESSIN        //////////////////////////////

 @Override
 public void draw(){
    
  background(230);
  quadrillage();
  strokeWeight(2);
    
  for(Fig f : pz) f.affiche();
}
 
@Override
public void mousePressed(){
    int mx=mouseX/DIMCASE,my=mouseY/DIMCASE;

    figCour=matrice[mx][my];    
    if(figCour.allume) figCour.setAllume(false);
    else figCour.setAllume(true);
    
    if(mx!=0) {
        figCour=matrice[mx-1][my];    
        if(figCour.allume) figCour.setAllume(false);
        else figCour.setAllume(true);
    }
    
    if(mx!=LGCASES-1) {
        figCour=matrice[mx+1][my];    
        if(figCour.allume) figCour.setAllume(false);
        else figCour.setAllume(true);
    }
    
    if(my!=0) {
        figCour=matrice[mx][my-1];    
        if(figCour.allume) figCour.setAllume(false);
        else figCour.setAllume(true);
    }
    
    if(my!=HTCASES-1) {
        figCour=matrice[mx][my+1];    
        if(figCour.allume) figCour.setAllume(false);
        else figCour.setAllume(true);
    }
    
    this.redraw();
}      

/////////////////////////////////////  GESTION CLAVIER        ///////////////////////////////


 @Override
 public void keyPressed(){

   boolean toucheActive=false;
   switch(key){
   
      
      case 's': // Démarrer chute
            pz.clear();
            figure();
            this.redraw();
      break;  
       
    
      case 'f': // Test fin
            println( "fin ");  exit(); 
      break;    
      case 'r': // ROTATION
          toucheActive= true;
          if(figCour!=null && figCour.peutTourner() ){
            int etat=(figCour.etat+1)%4 ;
            figCour.oriente(etat) ;
            print( "rotation ");
        }         
      break;
      
      case 'm': // RABOTAGE
       if (figCour!=null)figCour.raboter();
      break;
      
      case 'l': 
       List<Fig> lar=new LinkedList();  
       for (Fig f : lesFigs) lar.add(f);
       for (Fig f : lar){
         f.raboter();
       }  
       for (Fig f : lesFigs) {
       
         if (f.y<HTCASES-1){
           f.depY(1);
         }
         else{
           lesFigs.remove(f);
         }  
       
        } 
      break;
      
      case 'a':  // ZOOM AVANT
       DIMCASE++;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
       this.redraw();
      break;
       
      case 'd': // ZOOM ARRIERE
       DIMCASE--;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
        this.redraw();
      break;
  }
     
   switch(keyCode){
    
      case 37: // NAV GAUCHE
       toucheActive= true;     
       if(figCour!=null){
           if( !figCour.contactBordGauche() && !figCour.contactGaucheFigure()) { 
                figCour.depX(-1); print( "dep gauche ");
            }
            else if(figCour.contactBordGauche()){
                    print( "collision bord gauche ");
                  }
                  else{
                    print( "collision gauche  avec autre figure ");
                  }
       }
       
      break;
      case 39:   // NAV DROITE
        toucheActive= true;
        if(figCour!=null){
           if( !figCour.contactBordDroit() && !figCour.contactDroitFigure()) { 
                figCour.depX(1); print( "dep droite ");
            }
            else if(figCour.contactBordDroit()){
                    print( "collision bord droit ");
                  }
                  else{
                    print( "collision droite  avec autre figure ");
                  }
       }
      break; 
      
   }
   if (toucheActive)afficheFigSel();
   
}
   
 
//@Override
// public void mousePressed(){}        
 
}
